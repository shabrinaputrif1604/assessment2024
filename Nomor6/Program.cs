﻿Console.WriteLine(Palindrome("katak"));
Console.WriteLine(Palindrome("12021"));
Console.WriteLine(Palindrome("malam"));
Console.WriteLine(Palindrome("maling"));
Console.WriteLine(Palindrome("123321"));

bool Palindrome(string word)
{
    string first = "";
    string end = "";

    for (int i = 0; i <= word.Length/2; i++)
    {
        first += word[i];
        end += word[word.Length - i - 1];
    }

    return first == end;
}