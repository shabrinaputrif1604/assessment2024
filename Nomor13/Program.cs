﻿string clock = "2:20";

Console.WriteLine(TimeDegree(clock));

float TimeDegree(string value)
{
    string[] split = value.Split(":");
    float[] clockFloat = new float[split.Length];
    clockFloat = Array.ConvertAll(split, float.Parse);

    float degree = ((clockFloat[0] * 30) + (clockFloat[1]/60) * 30) - (clockFloat[1] * 6);
    return Math.Abs(degree);
}