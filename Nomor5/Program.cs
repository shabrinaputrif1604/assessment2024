﻿List<int> array = new List<int>(){ 0, 1 };
int n = 7;
int temp = 0;

for (int i = 0; i < n; i++)
{
    temp += array[i];
    array.Add(temp);
}

array.RemoveAt(0);
array.RemoveAt(0);

Console.WriteLine("\n" + String.Join(" ", array));