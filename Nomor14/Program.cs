﻿List<int> array = new List<int>() { 3, 9, 0, 7, 1, 2, 4 };
int n = 3;

Console.Write(String.Join(", ", ShiftElements(array, n)));

List<int> ShiftElements(List<int> array, int n)
{
    List<int> arrTemp = new List<int>();
    for (int i = 0; i < n; i++)
    {
        arrTemp.Add(array[0]);
        array.RemoveAt(0);
    }

    for (int i = 0; i < arrTemp.Count; i++)
    {
        array.Add(arrTemp[i]);
    }

    return array;
}