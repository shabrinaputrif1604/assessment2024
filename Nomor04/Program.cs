﻿int n = 7;

PrimeNumbers(n);

void PrimeNumbers(int n)
{
    List<int> prime = new List<int>();

    int count = 1;
    while (prime.Count != n)
    {
        if (IsPrime(count))
        {
            prime.Add(count);
        }
        count++;
    }

    Console.WriteLine(String.Join(" ", prime));
}

bool IsPrime(int n)
{
    if(n <= 1)
    {
        return false;
    }

    for(int i = 2; i < n; i++)
    {
        if(n % i == 0)
        {
            return false;
        }
    }

    return true;
}