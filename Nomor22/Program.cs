﻿List<int> candles = new List<int>() { 3, 3, 9, 6, 7, 20, 50 };
bool check = false;

List<int> fibonacci = Fibonacci(candles.Count);
while (!check)
{
    for (int i = 0; i < candles.Count; i++)
    {
        candles[i] -= fibonacci[i];
        if (candles[i] <= 0)
        {
            check = true;
        }
    }
}

Console.WriteLine($"Lilin ke-{candles.IndexOf(candles.Min()) + 1} adalah yang paling cepat habis");

List<int> Fibonacci(int n)
{
    List<int> list = new List<int>() { 0, 1 };
    List<int> newList = new List<int>();
    int temp = 0;
    for (int i = 0; i <= n; i++)
    {
        temp += list[i];
        list.Add(temp);
        newList.Add(temp);
    }
    newList.RemoveAt(0);

    return newList;
}