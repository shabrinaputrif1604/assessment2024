﻿List<int> array = new List<int>(){ 1, 2, 4, 7, 8, 6, 9 };

List<int> maxArray = array.GetRange(0, array.Count);
int totalMax = 0;

List<int> minArray = array.GetRange(0, array.Count);
int totalMin = 0;

for (int i = 0; i < 4; i++)
{
    int max = 0;
    for (int j = 0; j < maxArray.Count; j++)
    {
        if (maxArray[j] > max)
        {
            max = maxArray[j];
        }
    }
    totalMax += max;
    maxArray.Remove(max);

    int min = int.MaxValue;
    for (int j = 0; j < minArray.Count; j++)
    {
        if (minArray[j] < min)
        {
            min = minArray[j];
        }
    }
    totalMin += min;
    minArray.Remove(min);
}
Console.WriteLine(totalMax);
Console.WriteLine(totalMin);