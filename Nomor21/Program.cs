﻿List<char> track = new List<char>() { '_', '_', '_', '_', '_', 'O', '_', '_', '_' };
int st = 0;

for (int dist = 0; dist < track.Count; dist++)
{
    if (track[dist] == 'O' || track[dist+1] == 'O')
    {
        if(st >= 2)
        {
            st -= 2;
            dist += 2;
            Console.Write("J");
        }
        else
        {
            Console.WriteLine("FAILED!");
        }
    }
    else if (dist + 2 >= track.Count)
    {
        st -= 2;
        dist += 2;
        Console.Write("J");
    }
    else
    {
        st++;
        Console.Write("W");
    }
}