﻿List<char> moveA = new List<char>() { 'G', 'G', 'G' };
List<char> moveB = new List<char>() { 'G', 'K', 'B' };

int dist = 2;

int posA = 0, posB = dist + 1;

List<int> listPosA = new List<int>();
List<int> listPosB = new List<int>();

for (int i = 0; i < moveA.Count; i++)
{
    if ((moveA[i] == 'G' && moveB[i] == 'K') ||
        (moveA[i] == 'B' && moveB[i] == 'G') ||
        (moveA[i] == 'K' && moveB[i] == 'B'))
    {
        posA += 2;
        posB += 1;

        listPosA.Add(posA);
        listPosB.Add(posB);
    }
    else if ((moveB[i] == 'G' && moveA[i] == 'K') ||
             (moveB[i] == 'B' && moveA[i] == 'G') ||
             (moveB[i] == 'K' && moveA[i] == 'B'))
    {
        posA -= 1;
        posB -= 2;

        listPosA.Add(posA);
        listPosB.Add(posB);
    }
    
    Console.WriteLine("Posisi A = " + posA);
    Console.WriteLine("Posisi B = " + posB);

    if (Math.Abs(posA - posB) == 1)
    {
        if (Math.Abs((listPosA[listPosA.Count - 1] - listPosA[listPosA.Count - 2])) == 2)
        {
            Console.WriteLine("A menang!");
        }
        else if (Math.Abs((listPosB[listPosB.Count - 1] - listPosB[listPosB.Count - 2])) == 2)
        {
            Console.WriteLine("B menang!");
        }
        break;
    }

    if((i == moveA.Count - 1) && Math.Abs(posA - posB) != 1)
    {
        Console.WriteLine("Draw!");
    }
}