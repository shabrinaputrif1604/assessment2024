﻿DateTime start = new DateTime(2019, 1, 27, 5, 0, 1);
DateTime end = new DateTime(2019, 1, 28, 20, 0, 2);

TimeSpan diff = end - start;
Console.WriteLine(diff);

int park = 0;
if (diff.Days == 0)
{
    if ((diff.Hours >= 0 && diff.Minutes >= 0 && diff.Seconds >= 0) && (diff.Hours <= 8 && diff.Minutes >= 0 && diff.Seconds >= 0))
    {
        park = (diff.Hours * 1000) + 1000;
    }
}

if (diff.Hours >= 8 && diff.Minutes > 0 && diff.Seconds > 0 || diff.Days == 1 && diff.Hours == 0 && diff.Minutes == 0 && diff.Seconds == 0)
{
    park = 8000;
}

if((diff.Days == 1 && diff.Hours >= 0 && diff.Minutes >= 0 && diff.Seconds > 0) && diff.Days == 1 && diff.Hours <= 8 && diff.Minutes == 0 && diff.Seconds == 0)
{
    park = 15000;
}
else if(diff.Days >= 1 && diff.Hours >= 8 && diff.Minutes >= 0 && diff.Seconds > 0)
{
    if(diff.Hours >= 8)
    {
        park = 15000 + ((diff.Hours - 8) * 1000) + 1000;
    }
}
Console.WriteLine(park);