﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using System.Security.Policy;
using System.Security.Principal;

namespace api_lemak_resto.Models
{
    public class DB_RestoContext : DbContext
    {
        public DB_RestoContext(DbContextOptions<DB_RestoContext> options) : base(options)
        {
        }

        public DbSet<CartModel> CartModels { get; set; } = default!;
        public DbSet<RoleModel> RoleModels { get; set; } = default!;
        public DbSet<OrderModel> OrderModels { get; set; } = default!;
        public DbSet<UserModel> UserModels { get; set; } = default!;
        public DbSet<MenuModel> MenuModels { get; set; } = default!;
    }
}
