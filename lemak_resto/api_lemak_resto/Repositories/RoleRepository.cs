﻿using api_lemak_resto.Models;
using api_lemak_resto.utils;
using AutoMapper;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using viewmodel_lemak_resto;

namespace api_lemak_resto.Repositories
{
    public class RoleRepository : IRepository<RoleViewModel>
    {
        private DB_RestoContext db;
        private ResponseViewModel response = new ResponseViewModel();
        private IMapper mapper = GetMapper.UseMapper(typeof(RoleModel), typeof(RoleViewModel));
        
        public RoleRepository(DB_RestoContext _db)
        {
            db = _db;
        }

        public ResponseViewModel Create(RoleViewModel model)
        {
            try
            {
                RoleModel data = new RoleModel();
                data.Name = model.Name;
                data.CreatedBy = 1;
                data.CreatedDate = DateTime.Now;

                db.RoleModels.Add(data);
                db.SaveChanges();

                RoleViewModel dataVM = mapper.Map<RoleViewModel>(model);

                response.Message = "Success create role!";
                response.Success = true;
                response.Data = dataVM;
                return response;
            }
            catch
            {
                throw new Exception("Error when creating data.");
            }
        }

        public ResponseViewModel Delete(long id)
        {
            try
            {
                RoleModel dataModel = db.RoleModels.Where(a => a.IsDelete == false && a.Id == id).FirstOrDefault();
                db.RoleModels.Remove(dataModel);
                db.SaveChanges();

                response.Success = true;
                response.Message = "Success delete data!";
                return response;
            }
            catch
            {
                throw new Exception($"Data with id {id} is not found!");
            }

        }

        public ResponseViewModel GetAll()
        {
            List<RoleModel> dataModel = db.RoleModels.Where(a => a.IsDelete == false).ToList();
            List<RoleViewModel> dataVM = mapper.Map<List<RoleViewModel>>(dataModel);

            response.Success = true;
            response.Data = dataVM;
            response.Message = "Success get all data roles.";
            return response;
        }

        public ResponseViewModel GetById(long id)
        {
            RoleModel dataModel = db.RoleModels.Find(id);
            if(dataModel == null)
            {
                throw new Exception($"Data with id {id} is not found!");
            }

            RoleViewModel dataVM = mapper.Map<RoleViewModel>(dataModel);

            response.Success = true;
            response.Data = dataVM;
            response.Message = "Success get data role.";
            return response;
        }

        public ResponseViewModel SoftDelete(long id)
        {
            try
            {
                RoleModel dataModel = db.RoleModels.Where(a => a.IsDelete == false && a.Id == id).FirstOrDefault();
                dataModel.DeletedDate = DateTime.Now;
                dataModel.DeletedBy = 1;
                dataModel.IsDelete = true;

                db.RoleModels.Update(dataModel);
                db.SaveChanges();

                response.Success = true;
                response.Message = "Success soft delete data!";
                return response;
            }
            catch
            {
                throw new Exception($"Data with id {id} is not found!");
            }
        }

        public ResponseViewModel Update(RoleViewModel model)
        {
            try
            {
                RoleModel dataModel = db.RoleModels.FirstOrDefault(a => a.Id == model.Id && a.IsDelete == false);
                dataModel.Name = model.Name;
                dataModel.UpdatedBy = model.UpdatedBy;
                dataModel.UpdatedDate = model.UpdatedDate;

                db.RoleModels.Update(dataModel);
                db.SaveChanges();

                RoleViewModel dataVM = mapper.Map<RoleViewModel>(dataModel);

                response.Success = true;
                response.Message = "Success update data role!";
                response.Data = dataVM;
                return response;
            }
            catch
            {
                throw new Exception("Error when updating data.");
            }
        }
    }
}
