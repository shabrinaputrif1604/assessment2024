﻿using viewmodel_lemak_resto;

namespace api_lemak_resto.Repositories
{
    public interface IRepository<T>
    {
        ResponseViewModel GetAll();
        ResponseViewModel GetById(long id);
        ResponseViewModel Create(T model);
        ResponseViewModel Update(T model);
        ResponseViewModel Delete(long id);
        ResponseViewModel SoftDelete(long id);

    }
}
