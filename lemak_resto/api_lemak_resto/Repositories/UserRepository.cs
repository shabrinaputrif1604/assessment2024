﻿using api_lemak_resto.Models;
using api_lemak_resto.utils;
using AutoMapper;
using viewmodel_lemak_resto;

namespace api_lemak_resto.Repositories
{
    public class UserRepository : IRepository<UserViewModel>
    {
        private DB_RestoContext db;
        private ResponseViewModel response = new ResponseViewModel();
        private IMapper mapper = GetMapper.UseMapper(typeof(UserModel), typeof(UserViewModel));

        public UserRepository(DB_RestoContext _db)
        {
            db = _db;
        }

        public ResponseViewModel Create(UserViewModel model)
        {
            try
            {
                UserModel data = new UserModel();
                data.Name = model.Name;
                data.Email = model.Email;
                data.Password = model.Password;
                data.CreatedBy = 1;
                data.CreatedDate = DateTime.Now;

                db.UserModels.Add(data);
                db.SaveChanges();

                UserViewModel dataVM = mapper.Map<UserViewModel>(data);

                response.Success = true;
                response.Message = "Success!";
                response.Data = dataVM;
                return response;
            }
            catch
            {
                throw;
            }
        }

        public ResponseViewModel Delete(long id)
        {
            try
            {
                UserModel data = db.UserModels.Where(a => a.Id == id && a.IsDelete == false).FirstOrDefault();
                db.UserModels.Remove(data);
                db.SaveChanges();

                response.Success = true;
                response.Message = "Success!";
                return response;
            }
            catch
            {
                throw;
            }
        }

        public ResponseViewModel GetAll()
        {
            List<UserViewModel> data = (from u in db.UserModels
                                        select new UserViewModel
                                        {
                                            Id = u.Id,
                                            Name = u.Name,
                                            RoleId = u.RoleId.Id
                                        }).ToList();

            response.Success = true;
            response.Message = "Success get all data!";
            response.Data = data;
            return response;
        }

        public ResponseViewModel GetById(long id)
        {
            try
            {
                UserViewModel data = (from u in db.UserModels
                                      where u.Id == id
                                      select new UserViewModel
                                      {
                                          Id = u.Id,
                                          Name = u.Name,
                                          RoleId = u.RoleId.Id
                                      }).FirstOrDefault()!;

                response.Success = true;
                response.Message = "Success get data by id!";
                response.Data = data;
                return response;
            }
            catch
            {
                throw new ArgumentException("Data with id {id} is not found!");
            }
        }

        public ResponseViewModel SoftDelete(long id)
        {
            try
            {
                UserModel data = db.UserModels.Where(a => a.Id == id && a.IsDelete == false).FirstOrDefault();
                data.IsDelete = true;
                data.DeletedDate = DateTime.Now;
                data.DeletedBy = 1;

                db.UserModels.Update(data);
                db.SaveChanges();

                response.Success = true;
                response.Message = "Success";
                return response;
            }
            catch
            {
                throw;
            }
        }

        public ResponseViewModel Update(UserViewModel model)
        {
            try
            {
                UserModel data = db.UserModels.Where(a => a.Id == model.Id && a.IsDelete == false).FirstOrDefault();
                data.Name = model.Name;
                data.Email = model.Email;
                data.Password = model.Password;
                data.UpdatedDate = DateTime.Now;
                data.UpdatedBy = 1;

                db.UserModels.Update(data);
                db.SaveChanges();

                response.Success = true;
                response.Message = "Success";
                return response;
            }
            catch
            {
                throw;
            }
        }
    }
}
