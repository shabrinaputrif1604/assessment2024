﻿using api_lemak_resto.Models;
using api_lemak_resto.utils;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using NuGet.Packaging.Signing;
using NuGet.Protocol;
using viewmodel_lemak_resto;

namespace api_lemak_resto.Repositories
{
    public class MenuRepository : IRepository<MenuViewModel>
    {
        private DB_RestoContext db;
        private ResponseViewModel response = new ResponseViewModel();
        private IMapper mapper = GetMapper.UseMapper(typeof(MenuModel), typeof(MenuViewModel));
        public static readonly List<string> ImageExtensions = new() { ".jpg", ".bmp", ".png" };
        public MenuRepository(DB_RestoContext _db)
        {
            db = _db;
        }

        public ResponseViewModel Create(MenuViewModel model)
        {
            try
            {
                //Console.Write("TESTTTTTTTTTTTTTTTTTTTTTTT" + model.ToJson());
                MenuModel data = new MenuModel();
                if (model.ImageFile != null)
                {
                    var extension = Path.GetExtension(model.ImageFile.FileName);
                    if (ImageExtensions.Contains(extension.ToLower()))
                    {
                        using var dataStream = new MemoryStream();
                        model.ImageFile.CopyToAsync(dataStream);
                        byte[] image = dataStream.ToArray();
                        data.Image = image;
                    }
                    data.ImagePath = model.ImagePath;
                }
                data.Description = model.Description;
                data.Price = model.Price;
                data.Name = model.Name;
                data.CreatedBy = 1;
                data.CreatedDate = DateTime.Now;

                db.MenuModels.Add(data);
                db.SaveChanges();

                MenuViewModel dataVM = mapper.Map<MenuViewModel>(model);

                response.Success = true;
                response.Message = "Success!";
                response.Data = dataVM;
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResponseViewModel Delete(long id)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel GetAll()
        {
            List<MenuModel> dataModel = db.MenuModels.Where(a => a.IsDelete == false).ToList();
            List<MenuViewModel> dataVM = mapper.Map<List<MenuViewModel>>(dataModel);

            response.Success = true;
            response.Message = "Success get all data!";
            response.Data = dataVM;
            return response;
        }

        public ResponseViewModel GetById(long id)
        {
            try
            {
                MenuModel dataModel = db.MenuModels.Where(a => a.IsDelete == false).FirstOrDefault();
                MenuViewModel dataVM = mapper.Map<MenuViewModel>(dataModel);

                response.Success = true;
                response.Message = "Success get data by id!";
                response.Data = dataVM;
                return response;
            }
            catch
            {
                throw;
            }
        }

        public ResponseViewModel SoftDelete(long id)
        {
            try
            {
                MenuModel data = db.MenuModels.Where(a => a.IsDelete == false).FirstOrDefault();
                data.IsDelete = true;
                data.DeletedDate = DateTime.Now;
                data.DeletedBy = 1;

                response.Message = "Success!";
                response.Success = true;
                return response;
            }
            catch
            {
                throw;
            }
        }

        public ResponseViewModel Update(MenuViewModel model)
        {
            try
            {
                MenuModel data = new MenuModel();
                if (model.ImageFile != null)
                {
                    var extension = Path.GetExtension(model.ImageFile.FileName);
                    if (ImageExtensions.Contains(extension.ToLower()))
                    {
                        using var dataStream = new MemoryStream();
                        model.ImageFile.CopyToAsync(dataStream);
                        byte[] image = dataStream.ToArray();
                        data.Image = image;
                    }
                    data.ImagePath = model.ImagePath;
                }
                data.Description = model.Description;
                data.Price = model.Price;
                data.Name = model.Name;
                data.CreatedBy = 1;
                data.CreatedDate = DateTime.Now;

                db.MenuModels.Add(data);
                db.SaveChanges();

                MenuViewModel dataVM = mapper.Map<MenuViewModel>(model);

                response.Success = true;
                response.Message = "Success!";
                response.Data = dataVM;
                return response;
            }
            catch
            {
                throw;
            }
        }
    }
}
