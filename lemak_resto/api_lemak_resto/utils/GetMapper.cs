﻿using AutoMapper;

namespace api_lemak_resto.utils
{
    public class GetMapper
    {
        public static IMapper UseMapper(Type source, Type destination)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap(source, destination);
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }
    }
}
