﻿using api_lemak_resto.Models;
using api_lemak_resto.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using viewmodel_lemak_resto;

namespace api_lemak_resto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        MenuRepository _repo;
        public MenuController(DB_RestoContext db)
        {
            _repo = new MenuRepository(db);
        }

        [HttpPost]
        public ResponseViewModel Create([FromForm] MenuViewModel model)
        {
            return _repo.Create(model);
        }

        [HttpGet]
        public ResponseViewModel GetAll()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public ResponseViewModel GetById(long id)
        {
            return _repo.GetById(id);
        }

        [HttpPut]
        public ResponseViewModel Update([FromForm]MenuViewModel model)
        {
            return _repo.Update(model);
        }

        [HttpPut("{id}")]
        public ResponseViewModel SoftDelete(long id)
        {
            return _repo.SoftDelete(id);
        }

        [HttpDelete]
        public ResponseViewModel Delete(long id)
        {
            return _repo.Delete(id);
        }
    }
}
