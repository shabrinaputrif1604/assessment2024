﻿using api_lemak_resto.Models;
using api_lemak_resto.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using viewmodel_lemak_resto;

namespace api_lemak_resto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserRepository _repo;
        public UserController(DB_RestoContext db)
        {
            _repo = new UserRepository(db);
        }

        [HttpGet]
        public ResponseViewModel GetAll()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public ResponseViewModel GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPost]
        public ResponseViewModel Create(UserViewModel model)
        {
            return _repo.Create(model);
        }

        [HttpPut]
        public ResponseViewModel Update(UserViewModel model)
        {
            return _repo.Update(model);
        }

        [HttpPut("{id}")]
        public ResponseViewModel SoftDelete(long id)
        {
            return _repo.SoftDelete(id);
        }

        [HttpDelete("{id}")]
        public ResponseViewModel Delete(long id)
        {
            return _repo.Delete(id);
        }
    }
}
