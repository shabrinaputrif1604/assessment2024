﻿using Microsoft.AspNetCore.Mvc;
using viewmodel_lemak_resto;

namespace lemak_resto.Controllers
{
    public class MenuController : Controller
    {
        private readonly IWebHostEnvironment? _webHostEnvironment;
        public MenuController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public string Upload(IFormFile imageFile)
        {
            string uniqueFileName = "";

            if (imageFile != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + imageFile.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    imageFile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ResponseViewModel Create(MenuViewModel dataParam)
        {
            ResponseViewModel response = new ResponseViewModel();
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }

            response.Success = true;
            response.Message = "Success";
            response.Data = new
            {
                dataParam.ImagePath
            };

            return response;
        }

        public IActionResult Detail()
        {
            return View();
        }

        public IActionResult Update(MenuViewModel dataParam)
        {
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }
            return View();
        }
    }
}
