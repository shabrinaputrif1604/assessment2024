﻿using Microsoft.AspNetCore.Mvc;

namespace lemak_resto.Controllers
{
    public class RoleController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Update()
        {
            return View();
        }

        public IActionResult Detail()
        {
            return View();
        }
    }
}
