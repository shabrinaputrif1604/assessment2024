﻿DateTime start = new DateTime(2016, 2, 28);
DateTime end = new DateTime(2016, 3, 7);

int diff = (end - start).Days;

Dictionary<string, int> books = new Dictionary<string, int>()
{
    { "A", 14 },
    { "B", 3 },
    { "C", 7 },
    { "D", 7 }
};

foreach (var book in books)
{
    if (diff > book.Value)
    {
        int fine = (diff - book.Value) * 100;
        Console.WriteLine($"Denda buku {book.Key} adalah Rp.{fine}");
    }
    else
    {
        Console.WriteLine($"Buku {book.Key} tidak terkena denda");
    }
}