﻿string track = "NNTNNNTTTTTNTTTNTN";
Hattori(track);

void Hattori(string track)
{
    int calcTrack = 0, calcMount = 0, calcValley = 0;

    for (int i = 0; i < track.Length; i++)
    {
        //Console.Write(calcTrack + " ");
        if (track[i] == 'T')
        {
            calcTrack--;
            if (calcTrack == 0 && i != 0)
            {
                calcMount++;
            }
        }
        else if (track[i] == 'N')
        {
            calcTrack++;
            if (calcTrack == 0 && i != 0)
            {
                calcValley++;
            }
        }
    }

    Console.WriteLine($"Gunung berjumlah {calcMount} dan lembah berjumlah {calcValley}");
}
