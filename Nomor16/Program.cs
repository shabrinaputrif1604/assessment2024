﻿float[] price = { 42, 50, 30, 70, 30 };
int friends = 5;
int[] allergicFood = { 0, 1 };
int allergicFriends = 4;

float[] total = new float[friends];
for (int i = 0; i < price.Length; i++)
{
    float service = (float)(price[i] * (0.05));
    float tax = (float)(price[i] * (0.1));

    //if (i == allergicFood)
    if (allergicFood.Contains(i))
    {
        for (int j = 0; j < total.Length - allergicFriends; j++)
        {
            total[j] += (price[i] + service + tax) / (friends - allergicFriends);
        }
    }
    else
    {
        for (int j = 0; j < total.Length; j++)
        {
            total[j] += (price[i] + service + tax) / friends;
        }
    }
}

for (int i = 0; i < total.Length; i++)
{
    Console.WriteLine($"Total makan teman {i + 1} = " + total[i]);
}
