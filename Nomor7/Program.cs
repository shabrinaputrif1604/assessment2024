﻿//Mean, Median, Modus
using System.Text.Json;

int[] array = { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };

Sort(array);
Console.WriteLine("Nilai mean = " + Mean(array));
Console.WriteLine("Nilai median = " + Median(array));
Console.WriteLine("Nilai modus = " + Modus(array));

void Sort(int[] array)
{
    for (int i = 0; i < array.Length; i++)
    {
        for (int j = 0; j < array.Length - 1 - i; j++)
        {
            if (array[j] > array[j + 1])
            {
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}

int Mean(int[] array)
{
    int mean = 0;
    for (int i = 0; i < array.Length; i++)
    {
        mean += array[i];
    }
    //Console.WriteLine("Nilai mean = " + (mean / array.Length));
    return mean / array.Length;
}

int Median(int[] array)
{
    int median = 0;
    if (array.Length % 2 != 0)
    {
        median = array[array.Length / 2];
    }
    else
    {
        median = (array[array.Length / 2] + array[(array.Length - 1) / 2]) / 2;
    }
    //Console.WriteLine("Nilai median = " + median);
    return median;
}

int Modus(int[] array)
{
    Dictionary<int, int> numberCount = new Dictionary<int, int>();
    for (int i = 0; i < array.Length; i++)
    {
        if (numberCount.ContainsKey(array[i]))
        {
            numberCount[array[i]]++;
        }
        else
        {
            numberCount.Add(array[i], 1);
        }
    }

    int max = 0;
    int modus = 0;
    foreach (var item in numberCount)
    {
        if (item.Value > max)
        {
            max = item.Value;
            modus = item.Key;
        }
    }

    return modus;
}