﻿Console.WriteLine(Pangram("Sphinx of black quartz, judge my vow"));
Console.WriteLine(Pangram("A quick brown fox jumps over the lazy dog"));
Console.WriteLine(Pangram("The jay, pig, fox, zebra and my wolves!"));

bool Pangram(string sentence)
{
    string lowerSentence = sentence.ToLower();
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    HashSet<char> words = new HashSet<char>();

    for(int i = 0; i < lowerSentence.Length; i++)
    {
        if (alphabet.Contains(lowerSentence[i]))
        {
            words.Add(lowerSentence[i]);
        }
    }

    return words.Count == 26;
}